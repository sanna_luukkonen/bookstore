const mongoose = require("mongoose");

// Book Schema
const bookSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  genre: { type: String, required: true },
  description: { type: String },
  author: { type: String, required: true },
  publisher: { type: String },
  pages: { type: String },
  image_url: {
    type: String
  },
  buy_url: { type: String },
  create_date: { type: Date, default: Date.now }
});

const Book = (module.exports = mongoose.model("Book", bookSchema));

// get books
module.exports.getBooks = (callback, limit) => {
  Book.find(callback).limit(limit);
};

module.exports.getBookById = (id, callback) => {
  Book.findById(id, callback);
};

// Add book
module.exports.addBook = (book, callback) => {
  Book.create(book, callback);
};

// Update book
module.exports.updateBook = (id, book, options, callback) => {
  const query = { _id: id };
  const update = {
    title: book.title,
    genre: book.genre,
    description: book.description,
    pages: book.pages,
    author: book.author,
    publisher: book.publisher,
    image_url: book.image_url,
    buy_url: book.buy_url
  };
  Book.findOneAndUpdate(query, update, options, callback);
};

// Delete book
module.exports.deleteBook = (id, callback) => {
  const query = { _id: id };
  Book.deleteOne(query, callback);
};
