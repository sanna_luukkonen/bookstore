const express = require("express");

const app = express();

const bodyParser = require("body-parser");
const mongoose = require("mongoose");

app.use(bodyParser.json());

const Genre = require("./models/genre.js");
const Book = require("./models/book.js");

// Connect to mongoose
mongoose.connect("mongodb://localhost/bookstore", { useNewUrlParser: true });
const db = mongoose.connection;

app.get("/", (req, res) => {
  res.send("Please use /api/books or /api/genres");
});

app.get("/api/genres/", (req, res) => {
  Genre.getGenres((err, genres) => {
    if (err) {
      throw err;
    } else {
      res.json(genres);
    }
  });
});

app.post("/api/genres/", (req, res) => {
  const genre = req.body;
  Genre.addGenre(genre, (err, genre) => {
    if (err) {
      throw err;
    } else {
      res.json(genre);
    }
  });
});

app.put("/api/genres/:_id", (req, res) => {
  const id = req.params._id;
  const genre = req.body;
  // Set the new option to true so that Mongoose returns the document after the update
  Genre.updateGenre(
    id,
    genre,
    {
      new: true
    },
    (err, genre) => {
      if (err) {
        throw err;
      } else {
        res.json(genre);
      }
    }
  );
});

app.delete("/api/genres/:_id", (req, res) => {
  const id = req.params._id;

  Genre.deleteGenre(id, (err, genre) => {
    if (err) {
      throw err;
    } else {
      res.json(genre);
    }
  });
});

app.get("/api/books/", (req, res) => {
  Book.getBooks((err, books) => {
    if (err) {
      throw err;
    } else {
      res.json(books);
    }
  });
});

app.get("/api/books/:_id", (req, res) => {
  Book.getBookById(req.params._id, (err, book) => {
    if (err) {
      throw err;
    } else {
      res.json(book);
    }
  });
});

app.post("/api/books/", (req, res) => {
  const book = req.body;
  Book.addBook(book, (err, book) => {
    if (err) {
      throw err;
    } else {
      res.json(book);
    }
  });
});

app.put("/api/books/:_id", (req, res) => {
  const id = req.params._id;
  const book = req.body;
  Book.updateBook(
    id,
    book,
    {
      new: true
    },
    (err, book) => {
      if (err) {
        throw err;
      } else {
        res.json(book);
      }
    }
  );
});

app.get("/api/books/", (req, res) => {
  Book.getBooks((err, books) => {
    if (err) {
      throw err;
    } else {
      res.json(books);
    }
  });
});

app.delete("/api/books/:_id", (req, res) => {
  const id = req.params._id;

  Book.deleteBook(id, (err, result) => {
    if (err) {
      throw err;
    } else {
      res.json(result);
    }
  });
});

app.listen(3000, () => {
  console.log("Running on port 3000");
});
